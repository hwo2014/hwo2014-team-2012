#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

game_logic::game_logic()
  //: action_map
  //  {
  //    { "join", &game_logic::on_join },
  //    { "gameStart", &game_logic::on_game_start },
  //    { "carPositions", &game_logic::on_car_positions },
  //    { "crash", &game_logic::on_crash },
  //    { "gameEnd", &game_logic::on_game_end },
  //    { "error", &game_logic::on_error }
  //  }
{
	using namespace std::placeholders;

	action_map.insert(ActionMap::value_type("join", action_fun(std::bind(&game_logic::on_join, _1, _2))));
	action_map.insert(ActionMap::value_type("yourCar", action_fun(std::bind(&game_logic::on_your_car, _1, _2))));
	action_map.insert(ActionMap::value_type("gameInit", action_fun(std::bind(&game_logic::on_game_init, _1, _2))));
	action_map.insert(ActionMap::value_type("gameStart", action_fun(std::bind(&game_logic::on_game_start, _1, _2))));
	action_map.insert(ActionMap::value_type("carPositions", action_fun(std::bind(&game_logic::on_car_positions, _1, _2))));
	action_map.insert(ActionMap::value_type("crash", action_fun(std::bind(&game_logic::on_crash, _1, _2))));
	action_map.insert(ActionMap::value_type("spawn", action_fun(std::bind(&game_logic::on_spawn, _1, _2))));
	action_map.insert(ActionMap::value_type("lapFinished", action_fun(std::bind(&game_logic::on_lap_finished, _1, _2))));
	action_map.insert(ActionMap::value_type("dnf", action_fun(std::bind(&game_logic::on_dnf, _1, _2))));
	action_map.insert(ActionMap::value_type("finish", action_fun(std::bind(&game_logic::on_finish, _1, _2))));
	action_map.insert(ActionMap::value_type("gameEnd", action_fun(std::bind(&game_logic::on_game_end, _1, _2))));
	action_map.insert(ActionMap::value_type("tournamentEnd", action_fun(std::bind(&game_logic::on_tournament_end, _1, _2))));
	action_map.insert(ActionMap::value_type("error", action_fun(std::bind(&game_logic::on_error, _1, _1))));
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
	std::cout << "\tData: " << data << std::endl;
    return { make_ping() };
  }
}


game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}


game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
	std::cout << "Car: Name=\"" << data["name"].as<std::string>()
		<< "\" color=\"" << data["color"].as<std::string>() << "\"." << std::endl;
	return{ make_ping() };
}


game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
	std::cout << "Game Init" << std::endl;
	return{ make_ping() };
}


game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started." << std::endl;
  return { make_ping() };
}


game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  return { make_throttle(0.5) };
}


game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return { make_ping() };
}


game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
	std::cout << "Someone spawned" << std::endl;
	return{ make_ping() };
}


game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data)
{
	std::cout << "Lap finished" << std::endl;
	return{ make_ping() };
}


game_logic::msg_vector game_logic::on_dnf(const jsoncons::json& data)

{
	std::cout << "Disqualified!!!!!" << std::endl;
	return{ make_ping() };
}


game_logic::msg_vector game_logic::on_finish(const jsoncons::json& data)
{
	std::cout << "Someone finished" << std::endl;
	return{ make_ping() };
}


game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}


game_logic::msg_vector game_logic::on_tournament_end(const jsoncons::json& data)
{
	std::cout << "Tournament ended" << std::endl;
	return{ make_ping() };
}


game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}
